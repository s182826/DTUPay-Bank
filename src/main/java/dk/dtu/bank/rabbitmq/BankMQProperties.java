package dk.dtu.bank.rabbitmq;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *  @author Alexander V. Pedersen (s145099)
 */

@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
public class BankMQProperties {
    private String paymentExchange;
    private String paymentExchangePayQueue;
    private String paymentExchangePayRoutingKey;

    private String userExchange;
    private String userExchangeCreateUserQueue;
    private String userExchangeCreateUserRoutingKey;

    private String merchantExchange;
    private String merchantExchangeCreateMerchantQueue;
    private String merchantExchangeCreateMerchantRoutingKey;

    private String transactionExchange;
    private String transactionExchangeRefundQueue;
    private String transactionExchangeRefundRoutingKey;

    private String userUserExchange;
    private String userUserExchangeRoutingKey;

    private String transactionTransactionExchange;
    private String transactionTransactionExchangeRoutingKey;

    private String transactionRefundExchange;
    private String transactionRefundRoutingKey;


    public String getPaymentExchange() {
        return paymentExchange;
    }

    public void setPaymentExchange(String paymentExchange) {
        this.paymentExchange = paymentExchange;
    }

    public String getPaymentExchangePayQueue() {
        return paymentExchangePayQueue;
    }

    public void setPaymentExchangePayQueue(String paymentExchangePayQueue) {
        this.paymentExchangePayQueue = paymentExchangePayQueue;
    }

    public String getPaymentExchangePayRoutingKey() {
        return paymentExchangePayRoutingKey;
    }

    public void setPaymentExchangePayRoutingKey(String paymentExchangePayRoutingKey) {
        this.paymentExchangePayRoutingKey = paymentExchangePayRoutingKey;
    }

    public String getUserExchange() {
        return userExchange;
    }

    public void setUserExchange(String userExchange) {
        this.userExchange = userExchange;
    }

    public String getUserExchangeCreateUserQueue() {
        return userExchangeCreateUserQueue;
    }

    public void setUserExchangeCreateUserQueue(String userExchangeCreateUserQueue) {
        this.userExchangeCreateUserQueue = userExchangeCreateUserQueue;
    }

    public String getUserExchangeCreateUserRoutingKey() {
        return userExchangeCreateUserRoutingKey;
    }

    public void setUserExchangeCreateUserRoutingKey(String userExchangeCreateUserRoutingKey) {
        this.userExchangeCreateUserRoutingKey = userExchangeCreateUserRoutingKey;
    }

    public String getMerchantExchange() {
        return merchantExchange;
    }

    public void setMerchantExchange(String merchantExchange) {
        this.merchantExchange = merchantExchange;
    }

    public String getMerchantExchangeCreateMerchantQueue() {
        return merchantExchangeCreateMerchantQueue;
    }

    public void setMerchantExchangeCreateMerchantQueue(String merchantExchangeCreateMerchantQueue) {
        this.merchantExchangeCreateMerchantQueue = merchantExchangeCreateMerchantQueue;
    }

    public String getMerchantExchangeCreateMerchantRoutingKey() {
        return merchantExchangeCreateMerchantRoutingKey;
    }

    public void setMerchantExchangeCreateMerchantRoutingKey(String merchantExchangeCreateMerchantRoutingKey) {
        this.merchantExchangeCreateMerchantRoutingKey = merchantExchangeCreateMerchantRoutingKey;
    }

    public String getTransactionExchange() {
        return transactionExchange;
    }

    public void setTransactionExchange(String transactionExchange) {
        this.transactionExchange = transactionExchange;
    }

    public String getTransactionExchangeRefundQueue() {
        return transactionExchangeRefundQueue;
    }

    public void setTransactionExchangeRefundQueue(String transactionExchangeRefundQueue) {
        this.transactionExchangeRefundQueue = transactionExchangeRefundQueue;
    }

    public String getTransactionExchangeRefundRoutingKey() {
        return transactionExchangeRefundRoutingKey;
    }

    public void setTransactionExchangeRefundRoutingKey(String transactionExchangeRefundRoutingKey) {
        this.transactionExchangeRefundRoutingKey = transactionExchangeRefundRoutingKey;
    }

    public String getUserUserExchange() {
        return userUserExchange;
    }

    public void setUserUserExchange(String userUserExchange) {
        this.userUserExchange = userUserExchange;
    }

    public String getUserUserExchangeRoutingKey() {
        return userUserExchangeRoutingKey;
    }

    public void setUserUserExchangeRoutingKey(String userUserExchangeRoutingKey) {
        this.userUserExchangeRoutingKey = userUserExchangeRoutingKey;
    }

    public String getTransactionTransactionExchange() {
        return transactionTransactionExchange;
    }

    public void setTransactionTransactionExchange(String transactionTransactionExchange) {
        this.transactionTransactionExchange = transactionTransactionExchange;
    }

    public String getTransactionTransactionExchangeRoutingKey() {
        return transactionTransactionExchangeRoutingKey;
    }

    public void setTransactionTransactionExchangeRoutingKey(String transactionTransactionExchangeRoutingKey) {
        this.transactionTransactionExchangeRoutingKey = transactionTransactionExchangeRoutingKey;
    }

    public String getTransactionRefundExchange() {
        return transactionRefundExchange;
    }

    public void setTransactionRefundExchange(String transactionRefundExchange) {
        this.transactionRefundExchange = transactionRefundExchange;
    }

    public String getTransactionRefundRoutingKey() {
        return transactionRefundRoutingKey;
    }

    public void setTransactionRefundRoutingKey(String transactionRefundRoutingKey) {
        this.transactionRefundRoutingKey = transactionRefundRoutingKey;
    }
}
