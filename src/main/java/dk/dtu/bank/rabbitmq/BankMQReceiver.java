package dk.dtu.bank.rabbitmq;

import com.google.gson.Gson;
import dk.dtu.bank.libraries.BankServiceClient;
import dk.dtu.bank.libraries.wsdl.BankServiceException;
import dk.dtu.bank.libraries.wsdl.CreateAccountWithBalanceResponse;
import dk.dtu.bank.libraries.wsdl.GetAccountByCprNumberResponse;
import dk.dtu.bank.model.ingoing.BankCreateUserDTO;
import dk.dtu.bank.model.ingoing.BankTransferDTO;
import dk.dtu.bank.model.outgoing.TransactionDTO;
import dk.dtu.bank.model.outgoing.UserDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 *  @author Alexander V. Pedersen (s145099)
 */

@Log4j2
@Component
public class BankMQReceiver {
    @Autowired
    private BankServiceClient bankServiceClient;

    @Autowired
    BankMQSender bankMQSender;

    @RabbitListener(queues = {"${rabbitmq.userExchangeCreateUserQueue}","${rabbitmq.merchantExchangeCreateMerchantQueue}"})
    public void userQueueListener(final Message message) {
        String msg = new String(message.getBody());
        System.out.println("Received a new notification...");
        log.info("Listener received a create user message with json: " + msg);
        BankCreateUserDTO bankCreateUserDTO = new Gson().fromJson(msg, BankCreateUserDTO.class);
        System.out.println(bankCreateUserDTO.toString());
        try {
            CreateAccountWithBalanceResponse newAccount = bankServiceClient.createAccountWithBalance(
                    0,
                    bankCreateUserDTO.getCprNumber(),
                    bankCreateUserDTO.getFirstName(),
                    bankCreateUserDTO.getLastName());
            UserDTO userDTO = new UserDTO(newAccount.getReturn(), bankCreateUserDTO.getCprNumber());
            bankMQSender.sendUserID(userDTO);
        } catch (Exception e) {
            try {
                GetAccountByCprNumberResponse getAccountByCprNumberResponse = bankServiceClient.getAccountByCprNumber(bankCreateUserDTO.getCprNumber());
                UserDTO userDTO = new UserDTO(getAccountByCprNumberResponse.getReturn().getId(), bankCreateUserDTO.getCprNumber());
                bankMQSender.sendUserID(userDTO);
            } catch (Exception innerE) {
                throw new AmqpRejectAndDontRequeueException(innerE.getMessage());
            }
        }
    }

    @RabbitListener(queues = "${rabbitmq.transactionExchangeRefundQueue}")
    public void transactionQueueListener(final Message message) {
        String msg = new String(message.getBody());
        System.out.println("Received a new notification...");
        log.info("Listener received a refund message with json: " + msg);
        BankTransferDTO bankTransferDTO = new Gson().fromJson(msg, BankTransferDTO.class);
        System.out.println(bankTransferDTO.toString());

        if(transferMoney(bankTransferDTO)){
            bankMQSender.sendRefund(new TransactionDTO(bankTransferDTO.getAmount(),
                    bankTransferDTO.getDebtorCprNumber(),
                    bankTransferDTO.getCreditorCprNumber(),
                    bankTransferDTO.getDescription(),
                    LocalDate.now()));
        } else {
            String exceptionMessage = "Did not transfer money, transaction was cancelled.";
            throw new AmqpRejectAndDontRequeueException(exceptionMessage);
        }
    }

    @RabbitListener(queues = "${rabbitmq.paymentExchangePayQueue}")
    public void paymentQueueListener(final Message message) {
        String msg = new String(message.getBody());
        System.out.println("Received a new notification...");
        log.info("Listener received a payment message with json: " + msg);
        BankTransferDTO bankTransferDTO = new Gson().fromJson(msg, BankTransferDTO.class);
        System.out.println(bankTransferDTO.toString());

        if(transferMoney(bankTransferDTO)){
            bankMQSender.sendTransaction(new TransactionDTO(bankTransferDTO.getAmount(),
                    bankTransferDTO.getDebtorCprNumber(),
                    bankTransferDTO.getCreditorCprNumber(),
                    bankTransferDTO.getDescription(),
                    LocalDate.now()));
        } else {
            String exceptionMessage = "Did not transfer money, transaction was cancelled.";
            throw new AmqpRejectAndDontRequeueException(exceptionMessage);
        }
    }

    public boolean transferMoney(BankTransferDTO bankTransferDTO) {
        try {
            String debtorAccountNo = bankServiceClient.getAccountByCprNumber(bankTransferDTO.getDebtorCprNumber()).getReturn().getId();
            String creditorAccountNo = bankServiceClient.getAccountByCprNumber(bankTransferDTO.getCreditorCprNumber()).getReturn().getId();
            bankServiceClient.transferMoney(
                    bankTransferDTO.getAmount(),
                    debtorAccountNo,
                    creditorAccountNo,
                    bankTransferDTO.getDescription());
        } catch (Exception e) {
            log.error("Error found while transferring money: " + e.getMessage());
            return false;
        }
        return true;
    }
}