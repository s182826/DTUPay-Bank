package dk.dtu.bank.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *   @author Alexander V. Pedersen (s145099)
 */

@Configuration
public class BankMQConfig {
    @Autowired
    private BankMQProperties rabbitMQProperties;

    // Payment exchange
    @Bean(name = "PaymentExchange")
    TopicExchange paymentExchange() {
        return new TopicExchange(rabbitMQProperties.getPaymentExchange());
    }

    @Bean
    Binding payBinding(@Qualifier("PaymentQueue") Queue queue, @Qualifier("PaymentExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(rabbitMQProperties.getPaymentExchangePayRoutingKey());
    }

    @Bean(name = "PaymentQueue")
    Queue paymentQueue() {
        return new Queue(rabbitMQProperties.getPaymentExchangePayQueue(), true);
    }


    //User exchange
    @Bean(name = "UserExchange")
    TopicExchange userExchange() {
        return new TopicExchange(rabbitMQProperties.getUserExchange());
    }

    @Bean
    Binding createUserBinding(@Qualifier("UserCreatedQueue") Queue queue, @Qualifier("UserExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(rabbitMQProperties.getUserExchangeCreateUserRoutingKey());
    }

    @Bean(name = "UserCreatedQueue")
    Queue userCreatedQueue() {
        return new Queue(rabbitMQProperties.getUserExchangeCreateUserQueue(), true);
    }

    //Merchant exchange
    @Bean(name = "MerchantExchange")
    TopicExchange merchantExchange() {
        return new TopicExchange(rabbitMQProperties.getMerchantExchange());
    }

    @Bean
    Binding createMerchantBinding(@Qualifier("MerchantCreatedQueue") Queue queue, @Qualifier("MerchantExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(rabbitMQProperties.getMerchantExchangeCreateMerchantRoutingKey());
    }

    @Bean(name = "MerchantCreatedQueue")
    Queue merchantCreatedQueue() {
        return new Queue(rabbitMQProperties.getMerchantExchangeCreateMerchantQueue(), true);
    }


    //Transaction exchange
    @Bean(name = "TransactionExchange")
    TopicExchange transactionExchange() {
        return new TopicExchange(rabbitMQProperties.getTransactionExchange());
    }

    @Bean
    Binding createTransactionBinding(@Qualifier("TransactionRefundQueue") Queue queue, @Qualifier("TransactionExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(rabbitMQProperties.getTransactionExchangeRefundRoutingKey());
    }

    @Bean(name = "TransactionRefundQueue")
    Queue transactionRefundQueue() {
        return new Queue(rabbitMQProperties.getTransactionExchangeRefundQueue(), true);
    }

    /* Template and message converter
    @Bean
    public RabbitTemplate amqpTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

     */
}
