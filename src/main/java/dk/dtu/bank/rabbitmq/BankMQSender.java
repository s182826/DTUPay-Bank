package dk.dtu.bank.rabbitmq;

import com.google.gson.Gson;
import dk.dtu.bank.model.outgoing.TransactionDTO;
import dk.dtu.bank.model.outgoing.UserDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *  @author Alexander V. Pedersen (s145099)
 */

@Log4j2
@Component
public class BankMQSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    BankMQProperties rabbitMQProperties;

    public void sendUserID(UserDTO userDTO){
        System.out.println("Sent msg = " + userDTO.toString());
        log.info("Sending a create user message with object: " + userDTO.toString());
        rabbitTemplate.convertAndSend(rabbitMQProperties.getUserUserExchange(), rabbitMQProperties.getUserUserExchangeRoutingKey(), new Gson().toJson(userDTO));
    }

    public void sendTransaction(TransactionDTO transactionDTO){
        System.out.println("Sent msg = " + transactionDTO.toString());
        log.info("Sending a create transaction message with object: " + transactionDTO.toString());
        rabbitTemplate.convertAndSend(rabbitMQProperties.getTransactionTransactionExchange(), rabbitMQProperties.getTransactionTransactionExchangeRoutingKey(), new Gson().toJson(transactionDTO));
    }

    public void sendRefund(TransactionDTO transactionDTO){
        System.out.println("Sent msg = " + transactionDTO.toString());
        log.info("Sending a create refund message with object: " + transactionDTO.toString());
        rabbitTemplate.convertAndSend(rabbitMQProperties.getTransactionRefundExchange(), rabbitMQProperties.getTransactionRefundRoutingKey(), new Gson().toJson(transactionDTO));
    }
}
