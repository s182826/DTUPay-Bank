package dk.dtu.bank.model.ingoing;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
/**
 *  @author Alexander V. Pedersen (s145099)
 */
public class BankCreateUserDTO implements Serializable {
    private String cprNumber;
    private String firstName;
    private String lastName;

    public BankCreateUserDTO(@JsonProperty("cprNumber") String cprNumber,
                             @JsonProperty("firstName") String firstName,
                             @JsonProperty("lastName") String lastName) {
        this.cprNumber = cprNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getCprNumber() {
        return cprNumber;
    }

    public void setCprNumber(String cprNumber) {
        this.cprNumber = cprNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "BankCreateUserDTO{" +
                "cprNumber='" + cprNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
