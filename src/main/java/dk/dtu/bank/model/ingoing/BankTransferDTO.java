package dk.dtu.bank.model.ingoing;

import java.io.Serializable;

/**
 *  @author Alexander V. Pedersen (s145099)
 */

public class BankTransferDTO implements Serializable {
    private double amount;
    private String debtorCprNumber;
    private String creditorCprNumber;
    private String description;

    public BankTransferDTO(double amount, String debtorCprNumber, String creditorCprNumber, String description) {
        this.amount = amount;
        this.debtorCprNumber = debtorCprNumber;
        this.creditorCprNumber = creditorCprNumber;
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDebtorCprNumber() {
        return debtorCprNumber;
    }

    public void setDebtorCprNumber(String debtorCprNumber) {
        this.debtorCprNumber = debtorCprNumber;
    }

    public String getCreditorCprNumber() {
        return creditorCprNumber;
    }

    public void setCreditorCprNumber(String creditorCprNumber) {
        this.creditorCprNumber = creditorCprNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BankTransferDTO{" +
                "amount=" + amount +
                ", debtorCprNumber='" + debtorCprNumber + '\'' +
                ", creditorCprNumber='" + creditorCprNumber + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
