package dk.dtu.bank.model.outgoing;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 *  @author Alexander V. Pedersen (s145099)
 */

public class UserDTO implements Serializable {
    private String accountNo;
    private String cprNumber;

    public UserDTO(@JsonProperty("accountNo") String accountNo, @JsonProperty("cprNumber") String cprNumber) {
        this.accountNo = accountNo;
        this.cprNumber = cprNumber;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCprNumber() {
        return cprNumber;
    }

    public void setCprNumber(String cprNumber) {
        this.cprNumber = cprNumber;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "accountNo='" + accountNo + '\'' +
                ", cprNumber='" + cprNumber + '\'' +
                '}';
    }
}
