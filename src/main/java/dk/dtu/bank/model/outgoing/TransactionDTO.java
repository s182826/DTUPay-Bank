package dk.dtu.bank.model.outgoing;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *  @author Alexander V. Pedersen (s145099)
 */

public class TransactionDTO implements Serializable {
    private double amount;
    private String debtorCprNumber;
    private String creditorCprNumber;
    private String description;
    private LocalDate date;

    public TransactionDTO(@JsonProperty("amount") double amount,
                          @JsonProperty("debtorCprNumber") String debtorCprNumber,
                          @JsonProperty("creditorCprNumber") String creditorCprNumber,
                          @JsonProperty("description") String description,
                          @JsonProperty("date") LocalDate date) {
        this.amount = amount;
        this.debtorCprNumber = debtorCprNumber;
        this.creditorCprNumber = creditorCprNumber;
        this.description = description;
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDebtorCprNumber() {
        return debtorCprNumber;
    }

    public void setDebtorCprNumber(String debtorCprNumber) {
        this.debtorCprNumber = debtorCprNumber;
    }

    public String getCreditorCprNumber() {
        return creditorCprNumber;
    }

    public void setCreditorCprNumber(String creditorCprNumber) {
        this.creditorCprNumber = creditorCprNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "amount=" + amount +
                ", debtorCprNumber='" + debtorCprNumber + '\'' +
                ", creditorCprNumber='" + creditorCprNumber + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
