package dk.dtu.bank.libraries;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Configuration
public class BankServiceConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("dk.dtu.bank.libraries.wsdl");
        return marshaller;
    }

    @Bean
    public BankServiceClient bankServiceClient(Jaxb2Marshaller marshaller){
        BankServiceClient client = new BankServiceClient();
        client.setDefaultUri("http://fastmoney-00.compute.dtu.dk/BankService");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
