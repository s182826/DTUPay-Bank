package dk.dtu.bank.libraries;

import dk.dtu.bank.libraries.wsdl.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import javax.xml.bind.JAXBElement;
import java.math.BigDecimal;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public class BankServiceClient extends WebServiceGatewaySupport {

    public TransferMoneyFromToResponse transferMoney(double amount, String debtor, String creditor, String description){
        TransferMoneyFromTo transferMoneyFromTo = new ObjectFactory().createTransferMoneyFromTo();
        transferMoneyFromTo.setAmount(BigDecimal.valueOf(amount));
        transferMoneyFromTo.setDebtor(debtor);
        transferMoneyFromTo.setCreditor(creditor);
        transferMoneyFromTo.setDescription(description);

        JAXBElement<TransferMoneyFromTo> request = new ObjectFactory().createTransferMoneyFromTo(transferMoneyFromTo);

        JAXBElement<TransferMoneyFromToResponse> response = (JAXBElement<TransferMoneyFromToResponse>) getWebServiceTemplate().marshalSendAndReceive(request);

        return response.getValue();
    }

    public GetAccountResponse getAccount(String accountId){
        GetAccount getAccount = new ObjectFactory().createGetAccount();
        getAccount.setAccountId(accountId);

        JAXBElement<GetAccount> request = new ObjectFactory().createGetAccount(getAccount);

        JAXBElement<GetAccountResponse> response = (JAXBElement<GetAccountResponse>) getWebServiceTemplate().marshalSendAndReceive(request);

        return response.getValue();
    }

    public GetAccountByCprNumberResponse getAccountByCprNumber(String cprNumber){
        GetAccountByCprNumber getAccountByCprNumber = new ObjectFactory().createGetAccountByCprNumber();
        getAccountByCprNumber.setCpr(cprNumber);

        JAXBElement<GetAccountByCprNumber> request = new ObjectFactory().createGetAccountByCprNumber(getAccountByCprNumber);

        JAXBElement<GetAccountByCprNumberResponse> response = (JAXBElement<GetAccountByCprNumberResponse>) getWebServiceTemplate().marshalSendAndReceive(request);

        return response.getValue();
    }

    public CreateAccountWithBalanceResponse createAccountWithBalance(double amount, String CprNumber, String firstName, String lastName){
        CreateAccountWithBalance createAccountWithBalance = new ObjectFactory().createCreateAccountWithBalance();
        User user = new ObjectFactory().createUser();
        user.setCprNumber(CprNumber);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        createAccountWithBalance.setUser(user);
        createAccountWithBalance.setBalance(BigDecimal.valueOf(amount));

        JAXBElement<CreateAccountWithBalance> request = new ObjectFactory().createCreateAccountWithBalance(createAccountWithBalance);

        JAXBElement<CreateAccountWithBalanceResponse> response = (JAXBElement<CreateAccountWithBalanceResponse>) getWebServiceTemplate().marshalSendAndReceive(request);

        return response.getValue();
    }

    public RetireAccountResponse retireAccount(String accountId){
        RetireAccount retireAccount = new ObjectFactory().createRetireAccount();
        retireAccount.setAccountId(accountId);

        JAXBElement<RetireAccount> request = new ObjectFactory().createRetireAccount(retireAccount);

        JAXBElement<RetireAccountResponse> response = (JAXBElement<RetireAccountResponse>) getWebServiceTemplate().marshalSendAndReceive(request);

        return response.getValue();
    }

}
