package dk.dtu.bank;

import dk.dtu.bank.libraries.BankServiceClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * @author Christoffer Svendsen (s145089)
 */

@SpringBootTest
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
public class BankServiceTest {

    @Autowired
    private BankServiceClient client;

    private String accountIdDebtor;
    private String cprNumberDebtor = "1234-123456";
    private double amountDebtor = 1000;
    private double amountCreditor = 1000;
    private String accountIdCreditor;
    private String cprNumberCreditor = "4321-654321";

    @Before
    public void init(){
        client.createAccountWithBalance(amountDebtor, cprNumberDebtor, "Debtor", "Account");
        client.createAccountWithBalance(amountCreditor, cprNumberCreditor, "Creditor", "Account");
        accountIdDebtor = client.getAccountByCprNumber(cprNumberDebtor).getReturn().getId();
        accountIdCreditor = client.getAccountByCprNumber(cprNumberCreditor).getReturn().getId();
    }

    @After
    public void retireAccounts(){
        client.retireAccount(accountIdDebtor);
        client.retireAccount(accountIdCreditor);
    }

    @Test
    public void addTransaction(){
        double transferAmount = 200;
        client.transferMoney(transferAmount, accountIdDebtor, accountIdCreditor, "Test transfer");
        amountDebtor -= transferAmount;
        amountCreditor += transferAmount;
        assertEquals(BigDecimal.valueOf(amountDebtor), client.getAccountByCprNumber(cprNumberDebtor).getReturn().getBalance());
        assertEquals(BigDecimal.valueOf(amountCreditor), client.getAccountByCprNumber(cprNumberCreditor).getReturn().getBalance());
    }

    @Test
    public void getAccount(){
        assertEquals(accountIdDebtor, client.getAccount(accountIdDebtor).getReturn().getId());
        assertEquals(accountIdCreditor, client.getAccount(accountIdCreditor).getReturn().getId());
    }

    @Test
    public void getAccountByCpr(){
        assertEquals(cprNumberDebtor, client.getAccountByCprNumber(cprNumberDebtor).getReturn().getUser().getCprNumber());
        assertEquals(cprNumberCreditor, client.getAccountByCprNumber(cprNumberCreditor).getReturn().getUser().getCprNumber());
    }

}
