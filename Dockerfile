FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/bank-0.0.1-SNAPSHOT.jar /usr/src
EXPOSE 8084
CMD java -jar bank-0.0.1-SNAPSHOT.jar
